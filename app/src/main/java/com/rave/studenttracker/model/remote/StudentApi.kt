package com.rave.studenttracker.model.remote

import com.rave.studenttracker.model.dto.StudentDTO

/**
 * Student api.
 *
 * @constructor Create empty Student api
 */
interface StudentApi {

    /**
     * Fetch student list.
     *
     * @return
     */
    suspend fun fetchStudentList(): List<StudentDTO>
}
