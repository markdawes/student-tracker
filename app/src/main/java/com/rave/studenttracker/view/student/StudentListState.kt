package com.rave.studenttracker.view.student

import com.rave.studenttracker.model.entity.Student

/**
 * Student list state.
 *
 * @property isLoading
 * @property students
 * @constructor Create empty Student list state
 */
data class StudentListState(
    val isLoading: Boolean = false,
    val students: List<Student> = emptyList()
)
