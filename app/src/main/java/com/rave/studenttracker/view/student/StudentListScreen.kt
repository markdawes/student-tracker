package com.rave.studenttracker.view.student

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.rave.studenttracker.model.entity.Student

@Composable
fun StudentListScreen(students: List<Student>) {
    LazyColumn {
        items(
            items = students,
            key = { student: Student -> student.id }
        ) { student: Student ->
            StudentCard(student = student)
        }
    }
}

@Composable
fun StudentCard(student: Student) {
    val paddingModifier = Modifier.padding(10.dp)

    Card(border = BorderStroke(2.dp, Color.Black), modifier = paddingModifier) {
        AsyncImage(
            model = student.avatar,
            contentDescription = "student avatar"
            // modifier = Modifier.fillMaxWidth()
        )
        Text(
            text = "${student.firstName} ${student.lastName}\n${student.email}\n${student.university}",
            modifier = paddingModifier
        )
    }
}
