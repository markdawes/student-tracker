package com.rave.studenttracker.model

import com.rave.studenttracker.model.mapper.student.StudentMapper
import com.rave.studenttracker.model.remote.StudentApi
import com.rave.studenttracker.model.remote.StudentApiImpl
import com.rave.studenttracker.model.utilTest.CoroutinesTestExtension
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class StudentRepoTest {

    @RegisterExtension
    private val coroutinesTestExtension = CoroutinesTestExtension()
    val jsonString = "test"

    /*val jsonString = "[\n" +
            "  {\n" +
            "    \"id\": 1,\n" +
            "    \"first_name\": \"Whit\",\n" +
            "    \"last_name\": \"Padley\",\n" +
            "    \"email\": \"wpadley0@artisteer.com\",\n" +
            "    \"avatar\": \"https://robohash.org/sintperferendisvel.png?size=50x50&set=set1\",\n" +
            "    \"university\": \"Philippine Women's University\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": 2,\n" +
            "    \"first_name\": \"Darrel\",\n" +
            "    \"last_name\": \"Usherwood\",\n" +
            "    \"email\": \"dusherwood1@oakley.com\",\n" +
            "    \"avatar\": \"https://robohash.org/odiocumquequod.png?size=50x50&set=set1\",\n" +
            "    \"university\": \"Trinity College of Music\"\n" +
            "  }\n" +
            "]"*/

    val studentApi: StudentApi = StudentApiImpl(jsonString)
    val studentMapper = StudentMapper()
    private val repo = StudentRepo(studentApi, studentMapper)

    @Test
    @DisplayName("Testing that a list of students is returned")
    fun testGetStudentList() = runTest(coroutinesTestExtension.dispatcher) {
        // Given
        val result = listOf("Whit", "Darrel")

        // When
        val students = repo.getStudentList()

        // Then
        Assertions.assertEquals(result, listOf(students[0].firstName, students[1].firstName))
    }
}
